const express = require('express')
const app = express()
var json2xls = require('json2xls');
let node_xj = require("xls-to-json");
const fs = require('fs');

let input2014 = '2014';
let input2015 = '2015';

let e2014 = [];
let e2015 = [];
let finalResult = [];

const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

step1();

function getFileName1() {
    rl.question('insert the first xlsx file name (without the extention): ', (answer) => {
        input2014 = answer;
        console.log('user insert ' + input2014);        
        rl.question('insert the second xlsx file name (without the extention): ', (answer1) => {
            input2015 = answer1;
            console.log('user insert ' + input2015);
            step1();
            rl.close();
        });
      });
}

function step1() { 
    console.log('convert' + input2014 + 'file to json')
    node_xj({
        input: input2014 + ".xlsx",  // input xls
        output: input2014 + ".json" // output json
    }, function (err, result) {
        if (err) {
            console.error(err);
        } else {
            e2014 = result;
            step2()
        }
    });
}

function step2() { 
    console.log('convert' + input2015 + 'file to json')
    //convert 2015 to json
    node_xj({
        input: input2015 + ".xlsx",  // input xls
        output: input2015 + ".json" // output json
    }, function (err, result) {
        if (err) {
            console.error(err);
        } else {
            e2015 = result;
            step3();
        }
    });
}

function step3() {
    console.log('build the final object');

    finalResult = [{
        duplicate2014: [],
        duplicate2015: [],        
        unique: [],
        folder: 'התאמה מליאה',
        pathForUnique: 'ללא התאמה מליאה.xlsx',
        pathForFiltered: 'נמצאו מותאמים ב' + input2014 + '.xlsx',
        pathFor2015Filtered: 'נמצאו מותאמים ב' + input2015 + '.xlsx',        
        full2015: JSON.parse(JSON.stringify(e2015))
    },
    {
        duplicate2014: [],
        duplicate2015: [], 
        unique: [],
        folder: 'על פי שם וטלפון',
        pathForUnique: 'ללא התאמה.xlsx',
        pathForFiltered: 'נמצאו מותאמים ב' + input2014 + '.xlsx',
        pathFor2015Filtered: 'נמצאו מותאמים ב' + input2015 + '.xlsx',            
        full2015: JSON.parse(JSON.stringify(e2015))
    },
    {
        duplicate2014: [],
        duplicate2015: [], 
        unique: [],
        folder: 'על פי שם וכתובת',
        pathForUnique: 'ללא התאמה.xlsx',
        pathForFiltered: 'נמצאו מותאמים ב' + input2014 + '.xlsx',
        pathFor2015Filtered: 'נמצאו מותאמים ב' + input2015 + '.xlsx',            
        full2015: JSON.parse(JSON.stringify(e2015))
    },
    {
        duplicate2014: [],
        duplicate2015: [], 
        unique: [],
        folder: 'על פי שם וכתובת או שם וטלפון',
        pathForUnique: 'ללא התאמה.xlsx',
        pathForFiltered: 'נמצאו מותאמים ב' + input2014 + '.xlsx',
        pathFor2015Filtered: 'נמצאו מותאמים ב' + input2015 + '.xlsx',     
        full2015: JSON.parse(JSON.stringify(e2015))
    }]
    saveFullMatch();
}

function saveFullMatch() {
    for (let i = 0; i < e2014.length; i++) {
        let fullMatch = checkDuplicate(finalResult[0].full2015, e2014[i]);
        if (fullMatch === -1) {
            finalResult[0].unique.push(e2014[i]);
        } else {
            finalResult[0].duplicate2014.push(e2014[i]);
            finalResult[0].duplicate2015.push(finalResult[0].full2015[fullMatch]); 
            finalResult[0].full2015.splice(fullMatch, 1);
        }
    }

    finalResult[0].unique = finalResult[0].unique.concat(finalResult[0].full2015);
    step4();
}

function step4() {
    for (let i = 0; i < finalResult[0].unique.length; i++) {
        let nameAndPhone = compareNameAndPhone(finalResult[1].full2015, finalResult[0].unique[i]);
        let nameAndAddress = compareNameAndAddress(finalResult[2].full2015, finalResult[0].unique[i]);
        let phoneOrAddress = compareNameAndPhoneOrNameAndAddress(finalResult[3].full2015, finalResult[0].unique[i]);
        
        if (nameAndPhone === -1) {
            finalResult[1].unique.push(finalResult[0].unique[i]);
        } else {
            finalResult[1].duplicate2014.push(finalResult[0].unique[i]);
            finalResult[1].duplicate2015.push(finalResult[1].full2015[nameAndPhone]); 
            finalResult[1].full2015.splice(nameAndPhone, 1);
        }
        if (nameAndAddress === -1) {
            finalResult[2].unique.push(finalResult[0].unique[i]);
        } else {
            finalResult[2].duplicate2014.push(finalResult[0].unique[i]);
            finalResult[2].duplicate2015.push(finalResult[2].full2015[nameAndAddress]); 
            finalResult[2].full2015.splice(nameAndAddress, 1);
        }
        if (phoneOrAddress === -1) {
            finalResult[3].unique.push(finalResult[0].unique[i]);
        } else {
            finalResult[3].duplicate2014.push(finalResult[0].unique[i]);
            finalResult[3].duplicate2015.push(finalResult[3].full2015[phoneOrAddress]); 
            finalResult[3].full2015.splice(phoneOrAddress, 1);
        }
    }
    step5();
}

function step5() {
    finalResult.forEach((item, key) => {
        if(key !== 0) {
            item.unique = item.unique.concat(item.full2015);
        }
    });
    step6();
}

function step6() {
    finalResult.forEach((item) => {
        saveToXls(item.unique, item.duplicate2014, item.duplicate2015, item.pathForUnique, item.pathForFiltered, item.pathFor2015Filtered, item.folder);
    })
    console.log('done!');    
}

function saveToXls(jsonUnique, jsonDup, jsonDup2015, pathUnique, pathDup, pathDup2015, folder) {
    let jsonUniqueAsXls = json2xls(jsonUnique);
    let jsonDupAsXls = json2xls(jsonDup);
    let jsonDup2015AsXls = json2xls(jsonDup2015);

    if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder);
    }

    fs.writeFileSync(folder + '/' + pathUnique, jsonUniqueAsXls, 'binary');
    fs.writeFileSync(folder + '/' + pathDup, jsonDupAsXls, 'binary');
    fs.writeFileSync(folder + '/' + pathDup2015, jsonDup2015AsXls, 'binary');    
}

function checkDuplicate(arr, obj) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i]["משפחה + שם"] === obj["שם משפחה +פרטי"] &&
            arr[i]["מספר טלפון"] === obj["טלפון מסודר"] &&
            arr[i]["כתובת"] === obj["רחוב + מספר"]) {
            return i;
        }
    }
    return -1;
}

function compareNameAndPhone(arr, obj) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i]["משפחה + שם"] === obj["שם משפחה +פרטי"] && arr[i]["מספר טלפון"] === obj["טלפון מסודר"]) {
            return i;
        }
    }
    return -1;
}

function compareNameAndAddress(arr, obj) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i]["משפחה + שם"] === obj["שם משפחה +פרטי"] && arr[i]["כתובת"] === obj["רחוב + מספר"]) {
            return i;
        }
    }
    return -1;
}

function compareNameAndPhoneOrNameAndAddress(arr, obj) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i]["משפחה + שם"] === obj["שם משפחה +פרטי"] && arr[i]["מספר טלפון"] === obj["טלפון מסודר"] ||
            arr[i]["משפחה + שם"] === obj["שם משפחה +פרטי"] && arr[i]["מספר טלפון"] === obj["טלפון מסודר"]) {
            return i;
        }
    }
    return -1;
}
